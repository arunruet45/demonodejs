const nodemailer = require('nodemailer');

const transporter = () => nodemailer.createTransport({
    service: 'gmail',
    port: 587,
    secure: false,
    auth: {
        user: process.env.USER_EMAIL,
        pass: process.env.USER_PASSWORD
    }
});

const prepareMailOptionsJson = (receiver, title, body) => {
    return {
        from: process.env.USER_EMAIL,
        to: receiver,
        subject: title,
        text: body
    };
};

const sendmail = async (mailOptions) => {
    try {
        const data = await transporter().sendMail(mailOptions);
        console.log(data);
        return !!data;

    } catch (error) {
        return false;
    }
}

module.exports = {
    sendmail,
    prepareMailOptionsJson
}
