const mongoose = require('mongoose');
const {getAccessToken} = require('../Utilities/Utils');
const CONSTANT = require('../constants/Constant');
const sessionSchema = require('../schemas/sessionSchema');
const Session = new mongoose.model('Session', sessionSchema);

const generalResponseObject = (success, message) => {
    return {
        successful: success,
        message: message,
    }
};

const response = (res, message, statusCode, success) => {
    const response = generalResponseObject(success, message);
    return res.status(statusCode).json(response);
}

const success = (res, data) => {
    const response = {
        data: data || {},
        successful: true
    }
    return res.status(200).json(response);
};

const serverError = (res, err) => {
    return response(res, err, 500, false);
};

const badRequest = (res, message) => {
    return response(res, message, 400, false);
};

const alreadyExistEntity = (res, message) => {
    return response(res, message, 422, false);
};

const authenticationError = (res, message) => {
    return response(res, message, 401, false);
}

const unauthorized = (res, message) => {
    return response(res, message, 401, false);
}

const authenticationResponse = async (res, email) => {
    try {
        const previousSession = await Session.find({email: email});
        if (previousSession && previousSession.length > 0) {
            await Session.deleteOne({email: email})
        }

        const uuid = Date.now();
        const token = getAccessToken(email, uuid);
        if (token) {
            const session = new Session({
                email: email,
                uuid: uuid,
                token: token,
            });

            await session.save((err) => {
                if (err) {
                    return serverError(res, err);
                } else {
                    const data = {
                        accessToken: getAccessToken(email, uuid)
                    }
                    return success(res, data);
                }
            })

        } else {
            return serverError(res, CONSTANT.TEXT.TOKEN_GENERATION_ERROR);
        }
    } catch (error) {
        return serverError(res, CONSTANT.TEXT.SERVER_ERROR);
    }

}

module.exports = {
    serverError,
    success,
    badRequest,
    alreadyExistEntity,
    generalResponseObject,
    authenticationError,
    unauthorized,
    authenticationResponse
}