const jwt = require('jsonwebtoken');

const getAccessToken = (userUniqueIdentification, uuid) => {
    return jwt.sign({
        userUniqueIdentification: userUniqueIdentification,
        uuid: uuid
    }, process.env.JWT_SECRET);
}

const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

module.exports = {
    getAccessToken,
    validateEmail
}