const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const userHandler = require('./routerHandler/userHandler');

const app = express();
app.config = {port: 3000};
dotenv.config();
app.listen(app.config.port, () => {
    console.log('listening on port 3000')
})
app.use(cors())
app.use(express.json());

mongoose.connect('mongodb://localhost/demoNodejs',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log('DB connection successful'))
    .catch((error) => console.log('DB error', error));

app.use('/user', userHandler);

