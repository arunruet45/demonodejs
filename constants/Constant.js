const TEXT = Object.freeze({
    LOGIN_SUCCESSFUL: 'Login successful',
    SIGNUP_SUCCESSFUL: 'Signup successful',
    MISSING_PAYLOAD_DATA: 'Missing payload data',
    EMAIL_ALREADY_EXIST: 'Email already Exist',
    INVALID_EMAIL: 'Invalid Email',
    ON_LOGIN: 'on login',
    ON_SIGNUP: 'on signup',
    ON_CONTACT: 'on contact',
    USER_DOES_NOT_EXIST: 'user does not exist',
    LOGOUT_ERROR: 'logout error',
    AUTHENTICATION_FAILED: 'Authentication failed',
    AUTHORIZATION_FAILED: 'Authorization failed',
    TOKEN_GENERATION_ERROR: 'Token generation error',
    SERVER_ERROR: 'server error',
    MAIL_SENDING_ERROR: 'mail sending error'
});

const CONSTANT = {
    TEXT: TEXT
}

module.exports = CONSTANT