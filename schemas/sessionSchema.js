const mongoose = require('mongoose')

const sessionSchema = mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    uuid: {
        type: String,
        required: true,
    },
    token: {
        type: String,
        required: true
    },
})

module.exports = sessionSchema;