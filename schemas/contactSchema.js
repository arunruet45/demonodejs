const mongoose = require('mongoose')

const contactSchema = mongoose.Schema({
    receiver: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    dateTime: {
        type: Date,
        required: true
    }

})

module.exports = contactSchema;