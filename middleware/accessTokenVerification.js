const jwt = require('jsonwebtoken');
const mongoose = require('mongoose')
const {unauthorized} = require('../Utilities/responseHandler')
const CONSTANT = require('../constants/Constant');
const sessionSchema = require('../schemas/sessionSchema');
const Session = new mongoose.model('Session', sessionSchema);

const verifyAccessToken = async (req, res, next) => {
    const authHeader = req.headers['authorization'];
    if (!authHeader) {
        return next(unauthorized(res, CONSTANT.TEXT.AUTHORIZATION_FAILED));
    }

    const bearerToken = authHeader.split(' ');
    const token = bearerToken && bearerToken[1];
    if (token) {
        jwt.verify(token, process.env.JWT_SECRET, (err, payload) => {
            if (err) {
                return next(unauthorized(res, CONSTANT.TEXT.AUTHORIZATION_FAILED));
            }
            req.payload = payload;
        })

        const {userUniqueIdentification} = jwt.decode(token, process.env.JWT_SECRET);
        const userSession = await Session.find({email: userUniqueIdentification})
        if (userSession && userSession.length > 0) {
            next();

        } else {
            return next(unauthorized(res, CONSTANT.TEXT.AUTHORIZATION_FAILED));
        }

    } else {
        return next(unauthorized(res, CONSTANT.TEXT.AUTHORIZATION_FAILED));
    }
};

module.exports = {
    verifyAccessToken
}