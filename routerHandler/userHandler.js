const express = require('express');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const router = express.Router();
const userSchema = require('../schemas/userSchema');
const contactSchema = require('../schemas/contactSchema');
const User = new mongoose.model('User', userSchema);
const Contact = new mongoose.model('Contact', contactSchema);
const sessionSchema = require('../schemas/sessionSchema');
const Session = new mongoose.model('Session', sessionSchema);
const {serverError, success, badRequest, alreadyExistEntity, authenticationError, authenticationResponse} = require('../Utilities/responseHandler');
const {validateEmail} = require('../Utilities/Utils');
const CONSTANT = require('../constants/Constant');
const {verifyAccessToken} = require('../middleware/accessTokenVerification');
const {sendmail, prepareMailOptionsJson} = require('../services/sendMail')

router.post('/signup', async (req, res) => {
    try {
        const {fullName, email, password} = req.body;
        if (!fullName || !email || !password) {
            return badRequest(res, CONSTANT.TEXT.MISSING_PAYLOAD_DATA);
        }

        const isValidEmail = validateEmail(email);
        if (!isValidEmail) {
            return badRequest(res, CONSTANT.TEXT.INVALID_EMAIL);
        }

        const user = await User.find({email: email});
        if (user && user.length > 0) {
            return alreadyExistEntity(res, CONSTANT.TEXT.EMAIL_ALREADY_EXIST);
        }

        const hashedPassword = await bcrypt.hash(password, 10);
        const newUser = new User({
            fullName: fullName,
            email: email,
            password: hashedPassword
        });

        await newUser.save(async (err) => {
            if (err) {
                return serverError(res, err);

            } else {
                return authenticationResponse(res, email);
            }
        });

    } catch (err) {
        return serverError(res, err);
    }
});

router.post('/login', async (req, res) => {
    try {
        const {email, password} = req.body;

        if (!email || !password) {
            return badRequest(res, CONSTANT.TEXT.MISSING_PAYLOAD_DATA);
        }

        const isValidEmail = validateEmail(email);
        if (!isValidEmail) {
            return badRequest(res, CONSTANT.TEXT.INVALID_EMAIL);
        }

        const user = await User.find({email: email});
        if (user && user.length > 0) {
            const isValidPassword = await bcrypt.compare(password, user[0].password);

            if (isValidPassword) {
                return await authenticationResponse(res, email);
            } else {
                return authenticationError(res, CONSTANT.TEXT.AUTHENTICATION_FAILED)
            }

        } else {
            return authenticationError(res, CONSTANT.TEXT.AUTHENTICATION_FAILED)
        }

    } catch (err) {
        return serverError(res, err);
    }

})

router.post('/sendMail', verifyAccessToken, async (req, res) => {
    try {
        const {receiver, title, message} = req.body;
        if (!receiver || !message) {
            return badRequest(res, CONSTANT.TEXT.MISSING_PAYLOAD_DATA);
        }

        const isValidEmail = validateEmail(receiver.toLowerCase());
        if (!isValidEmail) {
            return badRequest(res, CONSTANT.TEXT.INVALID_EMAIL);
        }

        const mailOption = prepareMailOptionsJson(receiver, 'Contact mail', message);
        const isMailSent = await sendmail(mailOption);
        if (isMailSent) {
            const newContact = new Contact({
                receiver: receiver,
                title: title,
                message: message,
                dateTime: new Date()
            });

            await newContact.save((err) => {
                if (err) {
                    return serverError(res, err);

                } else {
                    return success(res);
                }
            });

        } else {
            return serverError(res, CONSTANT.TEXT.MAIL_SENDING_ERROR);
        }

    } catch (err) {
        return serverError(res, err);
    }
});

router.delete('/logout', verifyAccessToken, async (req, res) => {
    try {
        const {email} = req.query;

        if (!email) {
            return badRequest(res, CONSTANT.TEXT.MISSING_PAYLOAD_DATA);
        }

        const isValidEmail = validateEmail(email);
        if (!isValidEmail) {
            return badRequest(res, CONSTANT.TEXT.INVALID_EMAIL);
        }

        const user = await User.find({email: email});
        if (user && user.length > 0) {
            const userSession = await Session.find({email: email});

            if (userSession && userSession.length > 0) {
                await Session.deleteOne({email: email})
                return success(res);

            } else {
                return serverError(res, CONSTANT.TEXT.LOGOUT_ERROR);
            }

        } else {
            return serverError(res, CONSTANT.TEXT.USER_DOES_NOT_EXIST);
        }

    } catch (err) {
        return serverError(res, err);
    }
});

module.exports = router;